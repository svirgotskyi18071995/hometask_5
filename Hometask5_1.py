# Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.
tsk1 = '---------------------------------------- Task 1 ------------------------------------------------'
space = '------------------------------------------------------------------------------------------------'

def convert_to_float(value):
    if value.isdigit() or '.' in value:
        float_value = float(value)
        return float_value
    else:
        float_value = 0
        return float_value
my_input = input("Enter please your data.\n")
result = convert_to_float(my_input)
print(space)
print(f'Your result is ----> {result}\nType your result is ---->', (type(result)))
print(space)
