# Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік.
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
tsk3 = '---------------------------------------- Task 3 ------------------------------------------------'
space = '------------------------------------------------------------------------------------------------'
print(space)
print(tsk3)
print(space)
def checker(value):
    value.strip()
    if value.isdigit():
        int_value = int(value)
        if int_value >= 0:
            return int_value
    else:
        return 0
def print_options(option):
    str_option = str(option)
    if str_option.endswith('1'):
        scenarium_1 = 'рік'
        return scenarium_1
    elif str_option.endswith('2') or str_option.endswith('3') or str_option.endswith('4'):
        if 5 <= option <= 20:
            scenarium_3 = 'років'
            return scenarium_3
        else:
            scenarium_2 =  'роки'
            return scenarium_2
    else:
        scenarium_3 = 'років'
        return scenarium_3
def compare(user_age):
    str_user_age = str(user_age)
    if user_age < 7:
        res_1 = print(f'Тобі ж {user_age} {print_options(user_age)}, ! Де твої батьки?')
        return res_1
    elif 7 <= user_age < 16:
        if '7' in str_user_age:
            res_2_1 = print(f'Тобі лише {user_age} {print_options(user_age)}, а це фільм для дорослих! А також  вам пощастить')
            return res_2_1
        else:
            res_2 = print(f'Тобі лише {user_age} {print_options(user_age)}, а це фільм для дорослих!')
            return res_2

    elif user_age > 65:

        if '7' in str_user_age:
            res_3_1 = print(f'Вам {user_age} {print_options(user_age)}? Покажіть пенсійне посвідчення! А також  вам пощастить')
            return res_3_1
        else:
            res_3 = print(f'Вам {user_age} {print_options(user_age)}? Покажіть пенсійне посвідчення!')
            return res_3
    elif '7' in str_user_age:
        res_4 = print(f'Вам {user_age} {print_options(user_age)}, вам пощастить')
        return res_4
    else:
        res_5 = print(f'Незважаючи на те, що вам {user_age} {print_options(user_age)}, білетів всеодно нема!')
        return res_5


while True:
    input_age = input('Enter please your age.\n')
    result_checker = checker(input_age)
    if result_checker == 0:
        print('Error input! Try again!\n')
        continue
    else:
        age = result_checker
        break
print(space)
final_result = compare(age)
print(space)