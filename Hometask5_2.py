# Напишіть функцію, що приймає два аргументи. Функція повинна
# якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# у будь-якому іншому випадку повернути кортеж з цих аргументів
tsk2 = '---------------------------------------- Task 2 ------------------------------------------------'
space = '------------------------------------------------------------------------------------------------'
print(space)
print(tsk2)
print(space)
input1 = input('Enter please your first value.\n')
print(space)
input2 = input('Enter please your second value.\n')
print(space)
def calculation( first=input1, second=input2):
    if (first.isdigit() or '.' in first) and (second.isdigit()  or '.' in second):
        value_1 = float(first)
        value_2 = float(second)
        result = value_1 * value_2
        return result
    elif first.isalpha() and second.isalpha():
        result = first + second
        return result
    else:
        result_tuple = (first, second)
        return result_tuple

final_print = calculation(input1, input2)
print(f'Your result is ----> {final_print}')
print(space)
